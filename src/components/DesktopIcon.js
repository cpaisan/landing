import React, {Component} from 'react';
import Link from 'gatsby-link';
import resumePreview from '../static/images/resume-preview.png';
import '../styles/components/_desktopIcon.scss';

class DesktopIcon extends Component{

  render(){
    return (
      <div>
        <Link to='/resume'>
          <img className='preview' src={resumePreview} alt='resume-preview'></img>
        </Link>
        <h5 className='icon'><Link to='/resume'>{`Charlie-Paisan-resume.html`}</Link></h5>
      </div>
    )
  }
};

export default DesktopIcon;
