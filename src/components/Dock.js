import React, {Component} from 'react'
import '../styles/components/_dock.scss';
import $ from 'jquery';
import github from '../static/images/github.png';
import linkedIn from '../static/images/linkedin.png';
import mail from '../static/images/mail-icon.png';

class Dock extends Component{

  componentDidMount(){
    $(document).ready(function(){
      $("ul.dock li").each(function (type) {
          $(this).hover(function () {
              $(this).prev("li").addClass("nearby")
              $(this).next("li").addClass("nearby")
          },
          function () {
              $(this).prev("li").removeClass("nearby")
              $(this).next("li").removeClass("nearby")
          })
        })
    })
  }

  render(){
    const social = [
      {
        link: 'https://www.linkedin.com/in/charlie-paisan/',
        image: linkedIn,
        title: 'LinkedIn',
      },
      {
        link: 'https://github.com/cpaisan',
        image: github,
        title: 'Github',
      },
      {
        link: 'mailto:charlie.paisan@gmail.com',
        image: mail,
        title: 'Mail',
      }
    ]
    return(
      <div id="dockWrapper">
        <div className="dock-container left"></div>
        <ul className="dock">
          {
            social.map( (media) => (
              <li key={media.title}>
                <span>{media.title}</span>
                <a target='_blank' href={media.link} title={media.title}><img alt={media.title} src={media.image} /></a>
              </li>
            ))
          }
        </ul>
      </div>
    )
  }
};

export default Dock;
