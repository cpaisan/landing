import React, {Component} from 'react';
import Link from 'gatsby-link';
import '../styles/components/_terminal.scss';

class Terminal extends Component{
  render(){
    return(
      <div className='terminal-container'>
        <div className='terminal-header'>
          <div className='close-button'></div>
          <div className='minimize-button'></div>
          <div className='maximize-button'></div>
        </div>
        <pre className='terminal-text'>
          $ Skills
        </pre>
        <pre className='terminal-text'>
          $ Front-End: HTML5, CSS3 (SASS), JavaScript(ES5 ~ ES6), React, &nbsp;&nbsp;Angular.js, jQuery, Bootstrap, Handlebars
        </pre>
        <pre className='terminal-text'>
          {`$ Back-End: Node.js, Express, Python, Django`}
        </pre>
        <pre className='terminal-text'>
          {`$ Database: MySQL, MongoDB, Redis`}
        </pre>
      </div>
    )
  }
};


export default Terminal;
