import React from 'react'
import PropTypes from 'prop-types'
import Link from 'gatsby-link'
import Helmet from 'react-helmet'
import $ from 'jquery';
import "../styles/components/_header.scss";
import 'font-awesome/css/font-awesome.min.css';

// import './index.scss';

const pages = [
  {
    path: '/',
    text: 'Home'
  },
  {
    path: '',
    text: ''
  }
];

const Header = ({ location }) => {
  return (
    <div className={location.pathname === '/resume' ? 'header pull-up' : 'header'}>
      {
        pages.map( (page) => {
          if(location.pathname !== page.path){
            return <h5 className={location.pathname === '/resume' ? 'nav dark' : 'nav'} key={page.text}><Link to={page.path}>{page.text}</Link></h5>
          }
        })
      }
      {location.pathname === '/' ? <h1 className='name-header'>Charlie Paisan</h1> : null}
    </div>
  )
}

const TemplateWrapper = ({ children, location }) => {
  return (
    <div>
      <Helmet>
        <script
        src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous"></script>
        <title>Charlie Paisan</title>
        <script>
          {``}
        </script>
      </Helmet>
      <div
        style={location.pathname === '/resume' ? {
          margin: '0 auto',
          maxWidth: '100%',
          height: 'calc(100vh - 120px)'
        } : {
          margin: '0 auto',
          maxWidth: '100%',
          height: '100vh'
        }}
      >
        <Header location={location}/>
        {children()}
      </div>
    </div>
  )
}

TemplateWrapper.propTypes = {
  children: PropTypes.func,
}

export default TemplateWrapper
