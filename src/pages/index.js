import React from 'react';
import Link from 'gatsby-link';
import Dock from '../components/Dock';
import DesktopIcon from '../components/DesktopIcon';
import Terminal from '../components/Terminal';
import "../styles/components/_laptop.scss";

const IndexPage = () => (
  <section className="entry">
    <div className="greeting">
      <div className="laptop-wrapper">
        <div className="laptop">
          <div className='screen-bezel'>
            <div className="screen">
              <div className="description">
                {/*<h2 className="full-name">Hi, I'm Charlie Paisan</h2>
                <p className="personal-description">I am a product minded and detail oriented full-stack software engineer with a keen interest in building applications that connect people. I enjoy challenging myself with learning new technologies and sharing my knowledge with the community.</p>
                <div className="contact-header">
                  <h3>Contact</h3>
                </div>*/}
                <DesktopIcon/>
                <Terminal/>
                <Dock/>
                {/*<div className="contact-icons">
                  <a target="blank" href="https://www.linkedin.com/in/charlie-paisan/">
                    <i className="fa fa-4x fa-linkedin-square hvr-bounce-in icon" aria-hidden="true"></i>
                  </a>
                  <a target="blank" href="https://github.com/cpaisan/">
                    <i className="fa fa-4x fa-github hvr-bounce-in icon" aria-hidden="true"></i>
                  </a>
                  <a href="mailto:cppaisan@gmail.com">
                    <i className="fa fa-4x fa-envelope hvr-bounce-in icon" aria-hidden="true"></i>
                  </a>
                </div>*/}
              </div>
            </div>
          </div>
          <div className="keyboard"></div>
          <div className="bezel"></div>
        </div>
      </div>
    </div>
  </section>
)

export default IndexPage
