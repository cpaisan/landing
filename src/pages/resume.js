import React from 'react'
import Link from 'gatsby-link'
import "../styles/components/_resume.scss";

const Resume = () => (
  <main id="resume" className="page">
			<header className="resume-header clearfix">
			    <div className="profile-header pull-left">
			        <h1 className='resume-h1'>Charles Paisan</h1>
			        <h2 className='resume-h2'>Software Engineer</h2>
			    </div>
			</header>
			<div className="resume-content">
				<aside className="left-column">
					<div className="container about-container">
					    <div className="title">
					        <h3 className='resume-h3'>About</h3>
					        <div className="keyline"></div>
					    </div>


					        <div className="info-tag-container">
					            <i className="fa fa-envelope-o"></i>

					                <h6 className="info-text resume-h6">
					                    <a  className='resume' href="mailto:charlie.paisan@gmail.com" target="_blank">
					                        charlie.paisan@gmail.com
					                    </a>
					                </h6>
					        </div>

					        <div className="info-tag-container">
					            <i className="fa fa-mobile"></i>

					                <h6 className="info-text resume-h6">(626) 665-1225</h6>
					        </div>
                  <div className="info-tag-container">
                      <i className="fa fa-desktop"></i>

                          <h6 className="info-text resume-h6">
                              <a  className='resume' href="http://www.charliepaisan.com" target="_blank">
                                  www.charliepaisan.com
                              </a>
                          </h6>
                  </div>
                  <div className="info-tag-container">
                      <i className="fa fa-linkedin-square"></i>

                          <h6 className="info-text resume-h6">
                              <a  className='resume' href="https://www.linkedin.com/in/charlie-paisan/" target="_blank">
                                  in/charlie-paisan
                              </a>
                          </h6>
                  </div>
                   <div className="info-tag-container">
                       <i className="fa fa-github"></i>

                           <h6 className="info-text resume-h6">
                               <a  className='resume' href="https://github.com/cpaisan" target="_blank">
                                   cpaisan
                               </a>
                           </h6>
                   </div>
					</div>
					    <div className="skills-container">
					            <section className="container">
					                    <div className="title">
					                        <h3 className='resume-h3'>Front End</h3>
					                        <div className="keyline"></div>
					                    </div>
					                    <ul className="minimal resume">
					                            <li className='resume'><h6 className='resume-h6'>HTML5</h6></li>
					                            <li className='resume'><h6 className='resume-h6'>CSS3 (SASS)</h6></li>
					                            <li className='resume'><h6 className='resume-h6'>Javascript (ES5 ~ ES6)</h6></li>
					                            <li className='resume'><h6 className='resume-h6'>React</h6></li>
					                            <li className='resume'><h6 className='resume-h6'>Angular.JS</h6></li>
					                            <li className='resume'><h6 className='resume-h6'>JQuery</h6></li>
					                            <li className='resume'><h6 className='resume-h6'>Bootstrap</h6></li>
					                            <li className='resume'><h6 className='resume-h6'>Handlebars</h6></li>
					                    </ul>
					            </section>
					            <section className="container">
					                    <div className="title">
					                        <h3 className='resume-h3'>Back End</h3>
					                        <div className="keyline"></div>
					                    </div>
					                    <ul className="minimal resume">
					                            <li className='resume'><h6 className='resume-h6'>Node.js</h6></li>
					                            <li className='resume'><h6 className='resume-h6'>Express</h6></li>
					                            <li className='resume'><h6 className='resume-h6'>Python</h6></li>
					                            <li className='resume'><h6 className='resume-h6'>Django</h6></li>
					                    </ul>
					            </section>
					            <section className="container">
					                    <div className="title">
					                        <h3 className='resume-h3'>Database</h3>
					                        <div className="keyline"></div>
					                    </div>
					                    <ul className="minimal resume">
					                            <li className='resume'><h6 className='resume-h6'>MYSQL</h6></li>
					                            <li className='resume'><h6 className='resume-h6'>MongoDB</h6></li>
					                            <li className='resume'><h6 className='resume-h6'>Redis</h6></li>
					                    </ul>
					            </section>
					            <section className="container">
					                    <div className="title">
					                        <h3 className='resume-h3'>Software Dev</h3>
					                        <div className="keyline"></div>
					                    </div>
					                    <ul className="minimal resume">
					                            <li className='resume'><h6 className='resume-h6'>Automation</h6></li>
					                            <li className='resume'><h6 className='resume-h6'>CI/CD</h6></li>
					                            <li className='resume'><h6 className='resume-h6'>Cloud Services</h6></li>
					                            <li className='resume'><h6 className='resume-h6'>TDD</h6></li>
					                            <li className='resume'><h6 className='resume-h6'>GIT</h6></li>
					                    </ul>
					            </section>
					    </div>
				</aside>
				<div className="right-column">
					<div className="container work-container">
					    <div className="title">
					        <h3 className='resume-h3'>Experience</h3>
					        <div className="keyline"></div>
					    </div>

					        <section className="item">
					            <div className="section-header clearfix">
					                <h3 className="bold pull-left resume-h3">
					                        Next Generation Esports (NGE)
					                </h3>
					                <h5 className="italic pull-right resume-h5">
					                        <span className="startDate">04/2017</span>
					                        <span className="endDate"> - Present</span>
					                </h5>
					            </div>

					            <h4 className='resume-h4'>Team Lead/Front-End Engineer</h4>

					            <p className="summary resume">Lead architectural design for NGE Interactive&#x27;s web application on all levels of the technology stack as a Full Stack Engineer. Responsible for working cross-functionally across multiple teams to implement features and UX/UI design.</p>

					            <ul className='resume'>
					                <li className='resume'>Created a new web application based on NGE intellectual property, built with React, Node.js, and MySQL</li>
					                <li className='resume'>Successfully implemented strategic partner&#x27;s APIs to handle high volume time-sensitive money transactions</li>
					                <li className='resume'>Implemented Redis Pub/Sub solution to handle session data and keep all connected clients updated with the latest data</li>
					                <li className='resume'>Collaborated with the UX/UI Designer to design a modern and intuitive front-end UI experience using React</li>
					                <li className='resume'>Incorporated best practice Javascript design patterns to create an intuitive user experience and promote services oriented architecture</li>
					                <li className='resume'>Continue to drive NGE Interactive&#x27;s front-end development and implementation of new APIs</li>
					            </ul>
					        </section>
					        <section className="item">
					            <div className="section-header clearfix">
					                <h3 className="bold pull-left resume-h3">
					                        EMC
					                </h3>
					                <h5 className="italic pull-right resume-h5">
					                        <span className="startDate">01/2014</span>
					                        <span className="endDate"> - 11/2016</span>
					                </h5>
					            </div>

					            <h4 className='resume-h4'>Project Manager</h4>

					            <p className="summary resume">Utilized Agile/Scrum methodologies to manage Disaster Recovery solutions protecting enterprise cloud environments. Directed multi-discipline resources and maintained cross-functional relationships.</p>

					            <ul className='resume'>
					                <li className='resume'>Managed the deployment of enterprise storage and network appliances for mission critical applications to support customers’ immediate IT infrastructure requirements</li>
					                <li className='resume'>Developed workbooks and dashboards to analyze, forecast, manage and deliver a professional services portfolio of $15M+ annually</li>
					                <li className='resume'>Orchestrated the deployment of greenfield and brownfield hybrid cloud solutions</li>
					            </ul>
					        </section>
					        <section className="item">
					            <div className="section-header clearfix">
					                <h3 className="bold pull-left resume-h3">
					                        EMC
					                </h3>
					                <h5 className="italic pull-right resume-h5">
					                        <span className="startDate">06/2013</span>
					                        <span className="endDate"> - 12/2013</span>
					                </h5>
					            </div>

					            <h4 className='resume-h4'>Technology Solutions/Project Manager Intern</h4>

					            <p className="summary resume">Created Python scripts to generate timecard and expense data enabling sales opportunities and accurate revenue forecasts</p>

					        </section>
					</div>
					<div className="container education-container">
					    <div className="title">
					        <h3 className='resume-h3'>Education</h3>
					        <div className="keyline"></div>
					    </div>

					        <section className="item">
					            <div className="section-header clearfix">
					                <h3 className="bold pull-left resume-h3">
					                        University of California, Irvine
					                </h3>
					                <h5 className="italic pull-right resume-h5">
					                        <span className="startDate">09/2009</span>
					                        <span className="endDate"> - 06/2013</span>
					                </h5>
					            </div>

					            <h4 className='resume-h4'>B.A Business Economics</h4>


					        </section>
					</div>
				</div>
			</div>
		</main>
)

export default Resume
